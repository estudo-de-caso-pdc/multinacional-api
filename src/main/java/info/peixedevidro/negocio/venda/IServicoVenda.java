package info.peixedevidro.negocio.venda;


import info.peixedevidro.negocio.dto.VendaDto;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface IServicoVenda {
    String JNDI_NAME = "corbaname:iiop:peixedevidro.info:3700#java:global/Multinacional/ServicoVenda!info.peixedevidro.negocio.venda.IServicoVenda";


    List<VendaDto> listar();

    VendaDto obter(Long id);

    void excluir(Long id);

    VendaDto incluir(VendaDto incluirItem);

    VendaDto atualizar(VendaDto atualizarItem);
}
