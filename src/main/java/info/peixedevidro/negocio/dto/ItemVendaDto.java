package info.peixedevidro.negocio.dto;

import java.io.Serializable;

public class ItemVendaDto implements Serializable{
    private static final long serialVersionUID = 8457614923373598352L;

    private ItemDto item;
    private VendaDto venda;
    private Integer quantidade;

    public ItemDto getItem() {
        return item;
    }

    public void setItem(ItemDto item) {
        this.item = item;
    }

    public VendaDto getVenda() {
        return venda;
    }

    public void setVenda(VendaDto venda) {
        this.venda = venda;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}
