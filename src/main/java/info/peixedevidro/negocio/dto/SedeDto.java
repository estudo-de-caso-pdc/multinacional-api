package info.peixedevidro.negocio.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SedeDto implements Serializable{
    private static final long serialVersionUID = -2763298554943973079L;

    private Long id;
    private String nome;
    private String descricao;
    private List<VendaDto> vendaList = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<VendaDto> getVendaList() {
        return vendaList;
    }

    public void setVendaList(List<VendaDto> vendaList) {
        this.vendaList = vendaList;
    }
}
