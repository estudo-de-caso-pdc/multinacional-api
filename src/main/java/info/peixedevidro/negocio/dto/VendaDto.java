package info.peixedevidro.negocio.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class VendaDto implements Serializable {
    private static final long serialVersionUID = -89923827941305867L;

    private Long id;
    private SedeDto sede;
    private Date dia;
    private List<ItemVendaDto> itemVendaList = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SedeDto getSede() {
        return sede;
    }

    public void setSede(SedeDto sede) {
        this.sede = sede;
    }

    public Date getDia() {
        return dia;
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }

    public List<ItemVendaDto> getItemVendaList() {
        return itemVendaList;
    }

    public void setItemVendaList(List<ItemVendaDto> itemVendaList) {
        this.itemVendaList = itemVendaList;
    }
}
