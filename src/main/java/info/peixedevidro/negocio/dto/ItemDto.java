package info.peixedevidro.negocio.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ItemDto implements Serializable {
    private static final long serialVersionUID = 4895385851047770998L;

    private Long id;
    private String nome;
    private String descricao;
    private Integer estoque;
    private List<ItemVendaDto> itemVendaList = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getEstoque() {
        return estoque;
    }

    public void setEstoque(Integer estoque) {
        this.estoque = estoque;
    }

    public List<ItemVendaDto> getItemVendaList() {
        return itemVendaList;
    }

    public void setItemVendaList(List<ItemVendaDto> itemVendaList) {
        this.itemVendaList = itemVendaList;
    }
}
