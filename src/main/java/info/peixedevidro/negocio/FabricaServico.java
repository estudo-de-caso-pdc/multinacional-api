package info.peixedevidro.negocio;

import info.peixedevidro.negocio.item.IServicoItem;
import info.peixedevidro.negocio.venda.IServicoVenda;

import javax.ejb.EJBException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;

public class FabricaServico {
    public static IServicoVenda obterServicoVenda() {
        try {
            return (IServicoVenda) obterContext().lookup("info.peixedevidro.negocio.venda.IServicoVenda");
        } catch (NamingException e) {
            throw new EJBException("Não foi possível carregar o EJB ServicoVenda", e);
        }
    }

    public static IServicoItem obterServicoItem() {
        try {
            return (IServicoItem) obterContext().lookup("info.peixedevidro.negocio.item.IServicoItem");
        } catch (NamingException e) {
            throw new EJBException("Não foi possível carregar o EJB ServicoItem", e);
        }
    }

    private static InitialContext obterContext() throws NamingException {
        Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.enterprise.naming.SerialInitContextFactory");
        props.put(Context.URL_PKG_PREFIXES, "com.sun.enterprise.naming");
        props.put(Context.STATE_FACTORIES, "com.sun.corba.ee.impl.presentation.rmi.JNDIStateFactoryImpl");

        props.setProperty("org.omg.CORBA.ORBInitialHost", "peixedevidro.info");
        props.setProperty("org.omg.CORBA.ORBInitialPort", "3700");

        return new InitialContext(props);
    }
}
