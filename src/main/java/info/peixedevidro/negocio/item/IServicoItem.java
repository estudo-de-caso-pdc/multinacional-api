package info.peixedevidro.negocio.item;


import info.peixedevidro.negocio.dto.ItemDto;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface IServicoItem {
    String JNDI_NAME = "corbaname:iiop:peixedevidro.info:3700#java:global/Multinacional/ServicoItem!info.peixedevidro.negocio.item.IServicoItem";


    List<ItemDto> listar();

    ItemDto obter(Long id);

    void excluir(Long id);

    ItemDto incluir(ItemDto itemDto);

    ItemDto atualizar(ItemDto itemDto);
}
